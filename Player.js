/** Prototypes*/

// getMyPlayerRatio = require('./playerUtils.js').getMyPlayerRatio;

// let Player = function(name, attack, defense){
//     this.name = name;
//     this.attack = attack;
//     this.defense = defense;
//     this.hp = 100;
// }

// Player.prototype.displayMyPlayerInfo = function() {
//         console.log("My name is", 
//         this.name, 
//         ", I have",
//         this.attack,
//         " attack,",
//         this.defense,
//         " defense and",
//         this.hp,
//         " health points");
// }

// Player.prototype.fight = function(player2) {
//     var ratioPlayer1 = getMyPlayerRatio(this, player2.defense);
//     var ratioPlayer2 = getMyPlayerRatio(player2, this.defense);

//     if (ratioPlayer1 > ratioPlayer2) {
//         this.hp = ratioPlayer2 * (this.hp/ratioPlayer1);
//         player2.hp = 0;
//         return this.displayMyPlayerInfo();
//         } else if(ratioPlayer1 < ratioPlayer2) {
//         this.hp = 0;
//         player2.hp = ratioPlayer1 * (player2.hp/ratioPlayer2);
//         return player2.displayMyPlayerInfo();
//         }else{
//             return "Equality";
//         }
// }

// let PayToWinPlayer = function(name, attack, defense){
//     this.name = name;
//     this.attack = attack * 1.4;
//     this.defense = defense;
//     this.hp = 100;
// }

// PayToWinPlayer.prototype = Player.prototype;

// exports.Player = Player;
// exports.PayToWinPlayer = PayToWinPlayer;

/** Classes*/

// getMyPlayerRatio = require('./playerUtils.js').getMyPlayerRatio;

// class Player {
//     constructor(name, attack, defense) {
//         this.name = name;
//         this.attack = attack;
//         this.defense = defense;
//         this.hp = 100;
//     }
//     displayMyPlayerInfo() {
//         console.log("My name is", this.name, ", I have", this.attack, " attack,", this.defense, " defense and", this.hp, " health points");
//     }
//     fight(player2) {
//         var ratioPlayer1 = getMyPlayerRatio(this, player2.defense);
//         var ratioPlayer2 = getMyPlayerRatio(player2, this.defense);
//         if (ratioPlayer1 > ratioPlayer2) {
//             this.hp = ratioPlayer2 * (this.hp / ratioPlayer1);
//             player2.hp = 0;
//             return this.displayMyPlayerInfo();
//         }
//         else if (ratioPlayer1 < ratioPlayer2) {
//             this.hp = 0;
//             player2.hp = ratioPlayer1 * (player2.hp / ratioPlayer2);
//             return player2.displayMyPlayerInfo();
//         }
//         else {
//             return "Equality";
//         }
//     }
// }



// class PayToWinPlayer extends Player{
//     constructor(name, attack, defense) {
//         super(name, attack*1.4, defense);
//     }
// }


// exports.Player = Player;
// exports.PayToWinPlayer = PayToWinPlayer;


/** Promises */

const getMyPlayerRatio = require('./playerUtils.js').getMyPlayerRatio;
const requestPromise = require('request-promise')
const randomOrg = "https://www.random.org/integers/"


function randomNumber(min, max) {
    return requestPromise({
        url: randomOrg,
        qs: {
            num: 1,
            min: min,
            max: max,
            col: 1,
            base: 10,
            format: "plain"
        }
    })
    .then(output => parseInt(output.replace("\n", "")))
    .catch(err => console.error(err));
}

function randomSequence() {
    const sequence = [];
    let chaine = Promise.resolve(100);
    for (let i = 0; i < 10; i++) {
        chaine = chaine.then(last => randomNumber(1, Math.ceil(last * 1.2)));
        sequence.push(chaine);
    }

    const sequenceFilter = Promise.all(sequence).then(value => value.filter(number => number >= 10)
        .map((number, index) => {
            const coefficient = 1;
            if(number % 10 === index){
                coefficient = 2
            }
            return coefficient * number
        })
        .reduce((previousValue, currentValue) => previousValue + currentValue, 1)
    );
    return sequenceFilter.then(sum => sum / 100)
    .catch(err => console.error(err));
}

class Player {
    constructor (name, attack, defense) {
        this.name = name
        this.attack = attack
        this.defense = defense
        this.hp = 100
    }

    async fight(player2) {
        const randomSum1 = await randomSequence();
        const randomSum2 = await randomSequence();
        const ratioPlayer1 = getMyPlayerRatio(this, player2.defense) * randomSum1;
        const ratioPlayer2 = getMyPlayerRatio(player2, this.defense) * randomSum2;

        if (ratioPlayer1 > ratioPlayer2) {
            this.hp = ratioPlayer2 * (this.hp / ratioPlayer1);
            player2.hp = 0;            
            return this.displayMyPlayerInfo();
        }
        else if (ratioPlayer1 < ratioPlayer2) {
            this.hp = 0;
            player2.hp = ratioPlayer1 * (player2.hp / ratioPlayer2);
            return player2.displayMyPlayerInfo();
        }
        else {
            return "Equality";
        }
    }

    displayMyPlayerInfo() {
        console.log("My name is", this.name, ", I have", this.attack, " attack,", this.defense, " defense and", this.hp, " health points");
    }
}

class PayToWinPlayer extends Player{
    constructor(name, attack, defense) {
        super(name, attack*1.4, defense);
    }
}
    
    
exports.Player = Player;
exports.PayToWinPlayer = PayToWinPlayer;
    