getMyPlayerRatio = require('./playerUtils.js').getMyPlayerRatio;

exports.playerGenerator = (name, attack, defense) => {
    
    function displayMyPlayerInfo(){
        console.log("My name is", 
        this.name, 
        ", I have",
        this.attack,
        " attack,",
        this.defense,
        " defense and",
        this.hp,
        " health points");
    }

    function fight(player2){
        var ratioPlayer1 = getMyPlayerRatio(this, player2.defense);
        var ratioPlayer2 = getMyPlayerRatio(player2, this.defense);

        if (ratioPlayer1 > ratioPlayer2) {
            this.hp = ratioPlayer2 * (100/ratioPlayer1);
            player2.hp = 0;
            return this.playerInfo();
          } else if(ratioPlayer1 < ratioPlayer2) {
            this.hp = 0;
            player2.hp = ratioPlayer1 * (100/ratioPlayer2);
            return player2.playerInfo();
          }else{
              return "Equality";
          }
    }


    var obj = {
        name: name,
        attack: attack,
        defense: defense,
        playerInfo: displayMyPlayerInfo,
        hp: 100,
        fight: fight
    }

    /*
    var obj = {}
    this.name = name;
    this.attack = attack;
    this.defense = defense;
    this.playerInfo = displayMyPlayerInfo();
    */

    return obj;
}