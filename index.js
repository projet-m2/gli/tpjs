

// playerGenerator = require('./playerGenerator.js').playerGenerator;

// var player1 = playerGenerator("Monsieur Orc", "22", "5");
// var player2 = playerGenerator("Madame Elfe", "29","2")
// player1.fight(player2);
// player2.fight(player1);


/** Prototypes*/
// const player = require('./Player.js').Player;
// const payToWinPlayer = require('./Player.js').PayToWinPlayer;

// let player1 = new player("Le Nain", "13","9");
// let player2 = new payToWinPlayer("L'Elfe", "15", "5");


// player1.fight(player2);

/** Classes*/
const player = require('./Player.js').Player;
const payToWinPlayer = require('./Player.js').PayToWinPlayer;

let player1 = new player("L'Elfe", "15", "5");
let player2 = new payToWinPlayer("Le Nain", "13","9");
let player3 = new player("Le Nain sans or", "13", "9");
let player4 = new player("L'Homme", "17", "10");


player1.fight(player2);
player3.fight(player4);